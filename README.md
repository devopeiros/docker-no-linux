## Guia de Instalação: Docker no Ubuntu 

### Pré-requisitos
- Sistema operacional Ubuntu (versão X.XX ou superior)
- Privilégios administrativos

### Etapas:

1. Atualizar Pacotes do Sistema:
```
$ sudo apt update
```

2. Instalar Dependências do Docker:
```
$ sudo apt install apt-transport-https ca-certificates curl software-properties-common
```

3. Adicionar a Chave GPG Oficial do Docker:
```
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
```

4. Adicionar Repositório do Docker:
```
$ echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

5. Atualizar Pacotes do Sistema (Novamente):
```
$ sudo apt update
```

6. Instalar o **Docker Engine**:

```
$ sudo apt install docker-ce docker-ce-cli containerd.io
```

7. Verificar a Instalação do Docker:
```
$ sudo docker run hello-world
```

Se bem-sucedido, você deverá ver a mensagem "**Olá do Docker!**".

8. Gerenciar o Docker como um Usuário Não-Root (Opcional):
```
$ sudo usermod -aG docker $USER
```

Deslogue e faça login novamente para aplicar as alterações.

9. Instalação do Docker Concluída!

### Recursos Adicionais:
- [Documentação do Docker](https://docs.docker.com) 
- [Docker no GitLab](https://docs.gitlab.com/ee/topics/autodevops/container_registry.html#using-docker-in-gitlab-ci-yml)

Observe que os números de versão e comandos específicos podem variar. É sempre recomendável consultar a documentação oficial do Docker para obter as instruções mais atualizadas.